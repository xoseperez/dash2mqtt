/*

Amazon Dash to MQTT Bridge 0.0.1

The MIT License (MIT)

Copyright (C) 2016 by Xose Pérez <xose dot perez at gmail dot com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

 */

const config = require('./config.json');
const dash_button = require('node-dash-button');
const mqtt = require('mqtt');

var client = mqtt.connect(config.mqtt);
var dashes = Object.keys(config.dashes);
var dash = dash_button(dashes, null, null, 'all');

client.on('connect', function() {

    dash.on('detected', function (dash_id) {
        for (var mac in config.dashes) {
            if (dash_id == mac) {
                var dash = config.dashes[mac];
                console.log(dash.topic, dash.payload);
                client.publish(dash.topic, dash.payload);
            }
        }
    });

});
