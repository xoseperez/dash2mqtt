# Dash 2 MQTT

Amazon dash 2 MQTT bridge


## Install

You will first need to install libpcap in order to sniff for ARP/UD packages:

``` sh
$ sudo apt-get install libpcap-dev
```

Then just install the nodejs module dependencies:

``` sh
$ npm install
```

## Configure

Most of these information comes from node-dash-button package. You can check the source here: [https://github.com/hortinstein/node-dash-button/blob/master/README.md](https://github.com/hortinstein/node-dash-button/blob/master/README.md)

### First Time Dash Setup

Follow Amazon's instructions to configure your button to send messages when you push them but not actually order anything. When you get a Dash button, Amazon gives you a list of setup instructions to get going. Just follow this list of instructions, but don’t complete the final step (#3 I think) **Do not select a product, just exit the app**.

### Find a Dash

To find a dash on your network, run the following from the node-dash-button directory in node_modules:

``` sh
# you may need to use sudo due to libpcap running in permiscuous mode
$ cd node_modules/node-dash-button
$ node bin/findbutton
```

### Configure your MQTT mapping

Edit the ```config.json``` file and define your own configuration.
As you can see the ```dashes``` key is a dictionary of dash button MAC with each topic and payload to send.

## Run


``` sh
# you may need to use sudo due to libpcap running in permiscuous mode
$ npm start
```
